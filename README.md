# Helm 3 starter templates

The ``adstate`` helm starter template sets up a helm chart suitable for the CI/CD
environemnt at Serit:

* Scripts with ``Release`` and ``Debug`` deployment targets
* A standard CI/CD pipeline definition (edit as needed)
* Standard entrypoint and security scan scripts
* Review apps

### Install charts on Linux

```
cd ~/.local/share/helm
rm -rf starters
git clone git@itpartner.gitlab.no:adstate/helm-starters.git starters
```

### Usage

In your Maven/Java project:

```
helm create -p adstate {name}
mv {name} charts
```
